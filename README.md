# Other base16 color schemes

Color schemes for [chriskempson/base16](https://github.com/chriskempson/base16) by other people, where I didn't find a base16 repository:

- Ayu Mirage: Ike Ku (https://github.com/dempfi/ayu)
- Railscasts: Ryan Bates (http://railscasts.com)
